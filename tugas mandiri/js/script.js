var score = [0, 0, 0]; //variable

function vote(id) { //
  score[id] += 1;

  const element1 = document.getElementById("score-1"); //akses element 
  const element2 = document.getElementById("score-2"); //Selalu deklarasikan variabel dengan const 
  //ketika Anda tahu bahwa nilainya tidak boleh diubah.
  //A new Array
  // A new Object
  // A new Function
  // A new RegExp
  const element3 = document.getElementById("score-3");

  element1.innerHTML = score[0]; //html element
  element2.innerHTML = score[1];
  element3.innerHTML = score[2];

  console.log(score); //untuk debugging di browser 
}
